﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Object = System.Object;

public class CharacterInput : MonoBehaviour
{
    private PlayerInputActions _inputAction;
    public int playerNumber;
    [SerializeField]private Image icon;

    private bool enabled;
    
    private void Awake()
    {
        _inputAction = new PlayerInputActions();
    }

    private void OnEnable()
    {
        _inputAction.UI.Selection.performed += OnInteraction;
    }

    private void OnDisable()
    {
        _inputAction.UI.Selection.performed -= OnInteraction;
    }

    public void EnablePlayer(Transform newParent)
    {
        //Test for Fork
        enabled = true;
        icon.gameObject.SetActive(true);
        MovePlayer(newParent);
    }

    public void DisablePlayer()
    {
        enabled = false;
        icon.gameObject.SetActive(false);
    }

    private void OnInteraction(InputAction.CallbackContext context)
    {
        if (!enabled) return;
        var value = context.ReadValue<float>();
        PlayersOptions.PlayerInputTriggered?.Invoke(playerNumber, value);
    }

    public void MovePlayer(Transform newParent)
    {
        transform.parent = newParent;
    }
}
