﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayersOptions : MonoBehaviour
{
    [SerializeField]private Transform buttonParent;
    [SerializeField]private List<CharacterInput> playersInput = new List<CharacterInput>();
    private Dictionary<int, int> playersPosition = new Dictionary<int, int>();
    private List<Transform> optionTrans = new List<Transform>();

    public static Action<int, float> PlayerInputTriggered;

    private void OnEnable()
    {
        PlayerInputTriggered += TriggerPlayers;
        DisablePlayers();
    }

    private void OnDisable()
    {
        PlayerInputTriggered = null;
    }

    public void EnablePlayers()
    {
        foreach (Transform child in buttonParent)
        {
            optionTrans.Add(child);
        }
        
        foreach (var player in playersInput)
        {
            player.EnablePlayer(optionTrans[0]);
            playersPosition.Add(player.playerNumber, 0);
        }
    }

    public void DisablePlayers()
    {
        foreach (var player in playersInput)
        {
            player.DisablePlayer();
        }
    }
    
    private void TriggerPlayers(int player, float value)
    {
        playersInput[player]
            .MovePlayer(value > 0 ? optionTrans[CheckPosition(player, 1)] : optionTrans[CheckPosition(player, -1)]);
    }

    private int CheckPosition(int player, int inputValue)
    {
        var currentPosition = playersPosition[player];
        currentPosition += inputValue;
        if (currentPosition > optionTrans.Count)
        {
            currentPosition = 0;
        } else if (currentPosition < 0)
        {
            currentPosition = optionTrans.Count;
        }

        return currentPosition;
    }
}
